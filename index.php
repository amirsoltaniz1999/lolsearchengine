<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <meta name="description" content="Search the web for sites and images.">
	<meta name="keywords" content="Search engine, lol, websites">
	<meta name="author" content="Amir Soltani">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Welcome To LOL</title>
</head>
<body>
    <div class="wrapper indexPage">

        <div class="mainSection">

            <div class="logoContainer">
                <img src="assets/images/lollogo3.png" title="Logo of search engine" alt="engine logo">
            </div>

            <div class="searchContainer">

                <form action="search.php" method="GET">

                    <input class="searchBox" type="text" name="term">
                    <input class="searchButton" type="submit" value="Search">

                </form>

           
            </div>

        </div>
    </div>    
</body>
</html>